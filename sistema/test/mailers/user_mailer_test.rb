require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "notify_via_email" do
    mail = UserMailer.notify_via_email
    assert_equal "Notify via email", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
