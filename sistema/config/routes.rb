Rails.application.routes.draw do
    
    resources :comments
    resources :articles
    post 'articles/:id/comments', to: 'comments#create', as: :comment_article
    get 'articles/comment/:id', to: 'comments#edit', as: :edit_comment_article
    patch 'articles/:id/like', to: 'articles#like', as: :like_article
    patch 'articles/:id/deslike', to: 'articles#deslike', as: :deslike_article
    
    
    # Rotas de UserController
    get 'users/crud', to: 'users#crud', as: :crud
    get 'users', to: 'users#index', as: :users
    get 'users/new', to: 'users#new', as: :user_new
    post 'users/new', to: 'users#create'
    get 'users/:id', to: 'users#show', as: :user
    get 'users/edit/:id', to: 'users#edit', as: :user_edit
    patch 'users/edit/:id', to: 'users#update'
    delete 'users/destroy/:id', to: 'users#destroy', as: :user_destroy
    
    #Rotas de SessionsController
    get 'login', to: 'sessions#new', as: :login
    post 'login', to: 'sessions#create'
    delete 'logout',to: 'sessions#destroy', as: :logout
    
end
