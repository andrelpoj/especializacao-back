class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@boladao.com'
  layout 'mailer'
end
