class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.notify_via_email.subject
  #
  def notify_via_email(user,article)
    @user = user
    @article = article
    
    mail to: user.email, subject: "Novo Artigo Publicado!"
  end
end
