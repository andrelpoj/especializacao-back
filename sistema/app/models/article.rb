class Article < ApplicationRecord
  mount_uploader :photo, PictureUploader
  
  validates :title, length: { in: 2..50 }
  validates :content, length: { in: 2..500 }
  validate :picture_size
  
  belongs_to :user
  
  has_many :comments, dependent: :destroy
  has_many :like_articles, dependent: :destroy
  has_many :deslike_articles, dependent: :destroy
  
  after_create :deliver_mail
  
  private
  
  def deliver_mail
    @users = User.all
    @users.each do |user|
      UserMailer.notify_via_email(user, self).deliver_now
    end
  end
  
  def picture_size
    if photo.size > 2.megabytes
      errors.add(:photo, "Tá achando que é Google Drive?")
    end
  end
  
end
