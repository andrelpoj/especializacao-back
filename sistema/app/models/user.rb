class User < ApplicationRecord
  mount_uploader :profile_photo, PictureUploader
  
  has_secure_password
  
  validates :name, length: { in: 2..50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, length: { in: 10..50}, format: { with: VALID_EMAIL_REGEX }
  #allow_nil permite que voce nao precise redigitar a senha quando editar o usuario
  validates :password, length: { in: 3..6, presence: true, allow_nil: true }
  
  #validate pq a validacao foi definida por "nós"
  validate :picture_size
  
  
  has_many :articles, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :like_articles, dependent: :destroy
  has_many :deslike_articles, dependent: :destroy
  
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  
  private
  
  def picture_size
    if profile_photo.size > 2.megabytes
      errors.add(:profile_photo, "Tá achando que é Google Drive?")
    end
  end
  
end
