class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy,:like,:deslike]
  before_action :feeder, only: [:new,:create]
  before_action :non_logged_user
  before_action :correct_user, only: [:edit,:update]
  before_action :admin_or_correct_user, only: :destroy
  
  
  def like
    like = LikeArticle.find_by(article_id: @article.id, user_id: current_user.id)
    if like.nil?
      LikeArticle.create(article_id: @article.id, user_id: current_user.id)
    else
      like.destroy
    end
    @liked = LikeArticle.find_by(article_id: @article.id,user_id: current_user)
    deslike = DeslikeArticle.find_by(article_id: @article.id, user_id: current_user.id)
    deslike.destroy if deslike
    respond_to do |format|
      format.html { redirect_to @article }
      format.js
    end
  end
  
  def deslike
    deslike = DeslikeArticle.find_by(article_id: @article.id, user_id: current_user.id)
    if deslike.nil?
      DeslikeArticle.create(article_id: @article.id, user_id: current_user.id)
    else
      deslike.destroy
    end
    @desliked = DeslikeArticle.find_by(article_id: @article.id,user_id: current_user)
    like = LikeArticle.find_by(article_id: @article.id, user_id: current_user.id)
    like.destroy if like
    respond_to do |format|
      format.html { redirect_to @article }
      format.js
    end
  end

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @comment = Comment.new
    @comments = @article.comments
    @liked = LikeArticle.find_by(user_id: current_user.id, article_id: @article.id)
    @desliked = DeslikeArticle.find_by(user_id: current_user.id, article_id: @article.id)
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = current_user.articles.build(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:user_id, :title, :content, :photo)
    end
    
    def feeder
      if !current_user.feeder
        redirect_to articles_path 
      end
    end
    
    def correct_user
      if @article.user != current_user
        redirect_to articles_path
      end
    end
    
    def admin_or_correct_user
      if !current_user.admin && (@article.user != current_user)
        redirect_to articles_path
      end
    end
    
end
